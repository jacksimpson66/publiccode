#include "treeStructure.h"
#include "buildTree.h"
#include "writeTree.h"
#include "free.h"
#include "datadependent.h"

//this test tests that a full level two tree will be freed with no leaks
void task1test1() {
  Node *head;
  // make the head node
  head = makeNode( 0.0,0.0, 0 );

  makeChildren(head);

  writeTree(head);

  destroyTree(head);
}

//this test tests that a non uniform level three tree will be freed with no leaks
void task1test2() {
  Node *head;
  // make the head node
  head = makeNode( 0.0,0.0, 0 );

  makeChildren(head);

  makeChildren(head->child[1]);

  writeTree(head);

  destroyTree(head);
}

//creates full level two structure
void task2test1before() {
  Node *head;

  head = makeNode( 0.0,0.0, 0 );

  makeChildren(head);

  writeTree(head);

  destroyTree(head);
}

//grows from full level two structure using grow tree function
void task2test1after() {
  Node *head;

  head = makeNode( 0.0,0.0, 0 );

  makeChildren(head);

  growTree(head);

  writeTree(head);

  destroyTree(head);
}

//creates non uniform level 3 structure
void task2test2before() {
  Node *head;

  head = makeNode( 0.0,0.0, 0 );

  makeChildren(head);

  makeChildren(head->child[3]);

  writeTree(head);

  destroyTree(head);
}

//grows from non uniform level 3 tree using grow tree function
void task2test2after() {
  Node *head;

  head = makeNode( 0.0,0.0, 0 );

  makeChildren(head);

  makeChildren(head->child[3]);

  growTree(head);

  writeTree(head);

  destroyTree(head);
}

void task3test() {

  Node *head;

  head = makeNode( 0.0,0.0, 0 );

  makeChildren(head);

  makeChildren(head->child[3]);

  growTree(head);
  growTree(head);
  growTree(head);

  writeTree(head);

  destroyTree(head);

}

void task4test1() {
  Node *head;

  head = makeNode( 0.0,0.0, 0 );

  makeChildren(head);

  dataDependentTree(head, 0.5, 0);

  writeTree(head);

  destroyTree(head);
}

void task4test2() {
  Node *head;

  head = makeNode( 0.0,0.0, 0 );

  makeChildren(head);

  dataDependentTree(head, 0.2, 1);

  writeTree(head);

  destroyTree(head);
}

int main( int argc, char **argv ) {

  // task1test1();

  // task1test2();
  //
  // task2test1before();
  //
  // task2test1after();
  //
  // task2test2before();
  //
  // task2test2after();
  //
  // task3test();
  //
  // task4test1();
  //
  task4test2();

  return 0;
}
