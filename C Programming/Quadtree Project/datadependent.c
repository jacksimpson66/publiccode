#include "stdio.h"
#include "treeStructure.h"
#include "grow.h"
#include "valueTree.h"

#include "stdbool.h"

//global variable count
int count;

//initialises count to zero each time total nodes calculated
void initCount() {
  count = 0;
  return;
}

//counts nodes by visiting each one and incrementing counter
void countNodes(Node *node ) {
  int i;

  if( node->child[0] == NULL )
    count++;
  else {
    for ( i=0; i<4; ++i ) {
     countNodes( node->child[i] );
    }
    count++;
  }
  return;
}

//visits each leaf of the tree and tests whether to grow children or not
void addNodesOnData(Node *node, double tolerance, int choice ) {

  int i;

  if( node->child[0] == NULL ) {
    if(indicator(node,tolerance,choice)==0)
      makeChildren(node);
  }
  else {
    for ( i=0; i<4; ++i ) {
     addNodesOnData( node->child[i], tolerance, choice );
    }
  }
  return;

}


//counts nodes before and after nodes are added dependent on data. Continues to do so until no nodes need to be added
void dataDependentTree(Node *node, double tolerance, int choice) {

  int sizeBefore, sizeAfter;

  while(1) {
    initCount();
    countNodes(node);
    sizeBefore = count;
    addNodesOnData(node, tolerance, choice);

    initCount();
    countNodes(node);
    sizeAfter = count;

    if((sizeAfter - sizeBefore) == 0)
      return;
  }

  return;

}
