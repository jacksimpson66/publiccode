#include "treeStructure.h"
#include "stdio.h"
#include "stdlib.h"

//uses writeNode function and is changed so all nodes are freed, not just the children of active nodes

void destroyTree(Node *node ) {

  int i;

  if( node->child[0] == NULL )
    free(node);
  else {
    for ( i=0; i<4; ++i ) {
      destroyTree(node->child[i]);
    }
    free(node);
  }
  return;
}
