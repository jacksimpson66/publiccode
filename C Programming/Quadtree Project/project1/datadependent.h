#include "stdio.h"
#include "treeStructure.h"
#include "grow.h"
#include "valueTree.h"

#include "stdbool.h"

int countNodes(Node *node );

void addNodesOnData(Node *node, double tolerance, int choice );

void dataDependentTree(Node *node, double tolerance, int choice);
