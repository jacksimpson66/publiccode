#include "treeStructure.h"
#include "buildTree.h"
#include "writeTree.h"
#include "free.h"
#include "datadependent.h"

int main( int argc, char **argv ) {

  Node *head;

  // make the head node
  head = makeNode( 0.0,0.0, 0 );

  // make a tree

  growTree(head);
  growTree(head);
  growTree(head);
  growTree(head);

  growTree(head);

  writeTree(head);

  destroyTree(head);

  return 0;
}
