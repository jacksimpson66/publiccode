#include "treeStructure.h"
#include "stdio.h"
#include "stdlib.h"

//uses writeNode function to exponentially grow tree by one level

void growTree(Node *node ) {


  int i;
    if( node->child[0] == NULL )
      makeChildren(node);
    else {
      for ( i=0; i<4; ++i ) {
        growTree( node->child[i] );
      }
    }
    return;
}
