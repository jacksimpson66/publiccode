#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <stdbool.h>
#include "DataStructure.h"
#include "Free.h"
#include "menus.h"
#include "Print.h"
#include "unity.h"
#include "addRemove.h"
#include "borrowReturn.h"
#include "readWriteFile.h"

//declares pointers to beginning of linked lists meaning that they can be used by counting functions and test functions

Student *studentBgn;
Student *newBgnStudent;
Book *bookBgn;
Book *newBgnBook;

/*next 3 functions are declared in this test file rather than the actual program as they only serve a purpose in the test file
they iterate through the linked lists and return an integer value which relates to the size of the linked list 
final of the 3 functions only returns the size of the list where the books are available*/
int countListStudent(Student *bgn){
	int counter = 0;
	for (const Student *current = bgn; current != NULL; current = current->next)
    		counter++;
	return counter;
}

int countListBook(Book *bgn){
	int counter = 0;
	for (const Book *current = bgn; current != NULL; current = current->next)
    		counter++;
	return counter;
}

int countListAvailableBook(Book *bgn){
	int counter = 0;
	for (const Book *current = bgn; current != NULL; current = current->next) {
		if(current->status == 1)
    			counter++;
	}
	return counter;
}

//set up and tear down for each test, creates the linked lists to be used in the tests and also frees the memory afterwards - run with valgrind for proof
void setUp(void) {
	studentBgn = NULL;
	studentBgn = readFileStudent("students.csv", studentBgn);
	bookBgn = NULL;
	bookBgn = readFileBook("books.csv", bookBgn);
	newBgnBook = NULL;
	newBgnStudent = NULL;
}

void tearDown(void) {
	
	deleteBooks(bookBgn);
	deleteBooks(newBgnBook);
	deleteStudent(studentBgn);	
	deleteStudent(newBgnStudent);
}

//tests whether a student has been successfully added to list by comparing the size before the add and after
void test_addStudent(void) {
	int previous = countListStudent(studentBgn);
	studentBgn = addStudent("James", "Roy", studentBgn);
	TEST_ASSERT(countListStudent(studentBgn) == previous + 1);
}

//this is the same as the previous test but for books
void test_addBook(void) {
	int previous = countListBook(bookBgn);
	bookBgn = addBook("Test", "Book", bookBgn);
	TEST_ASSERT(countListBook(bookBgn) == previous + 1);
}

//same as before but for removing books rather than adding books
void test_removeBook(void) {
	int previous = countListBook(bookBgn);
	bookBgn = removeBook(findBook("Odyssey", bookBgn), bookBgn);
	TEST_ASSERT(countListBook(bookBgn) == previous - 1);
}

//tests that a true value is returned for when an accurate search is made and also false value is returned when name is spelt wrong
void test_searchBook(void) {
	bool searchResultTrue = searchBookName("Odyssey", bookBgn);
	bool searchResultFalse = searchBookName("odysey", bookBgn);
	TEST_ASSERT(searchResultTrue == true);
	TEST_ASSERT(searchResultFalse == false);
}

//tests that the amount of available books decreases by one when a book is borrowed
void test_borrowBook(void) {
	int previous = countListAvailableBook(bookBgn);
	borrowBook("Odyssey", bookBgn);
	TEST_ASSERT(countListAvailableBook(bookBgn) == previous - 1);
}

//borrows book initially, then returns it, and tests that available books have increased
void test_returnBook(void) {
	borrowBook("Odyssey", bookBgn);
	int previous = countListAvailableBook(bookBgn);
	returnBook("Odyssey", bookBgn);
	TEST_ASSERT(countListAvailableBook(bookBgn) == previous + 1);
}

//adds a student, writes to file, then creates new linked list and tests that when the file data is read into the list it increases in size by 1
void test_writeFileStudent(void) {
	int previous = countListStudent(studentBgn);
	studentBgn = addStudent("James", "Roy", studentBgn);
	writeFileStudent("students.csv", studentBgn);
	newBgnStudent = readFileStudent("students.csv", newBgnStudent);
	TEST_ASSERT(countListStudent(newBgnStudent) == previous + 1);
}

//same as before but for books
void test_writeFileBook(void) {
	int previous = countListBook(bookBgn);
	bookBgn = addBook("Test", "Book", bookBgn);
	writeFileBook("books.csv", bookBgn);
	newBgnBook = readFileBook("books.csv", newBgnBook);
	TEST_ASSERT(countListBook(newBgnBook) == previous + 1);
}	

int main(void) {
	UNITY_BEGIN();
	RUN_TEST(test_addStudent);
	RUN_TEST(test_addBook);
	RUN_TEST(test_removeBook);
	RUN_TEST(test_searchBook);
	RUN_TEST(test_borrowBook);
	RUN_TEST(test_returnBook);
	RUN_TEST(test_writeFileStudent);
	RUN_TEST(test_writeFileBook);
	return UNITY_END();
}



