#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include "DataStructure.h"

//used as counter which is incremented when new students are added, hence giving all students a unique ID number
int ID = 0;

/*this function takes a first name, a last name and a pointer to the beginning of the linked list as its parameters
it then dynamically allocates memory for a new node of the type student
then copies the first and last names into the node and gives unique ID
sets its next pointer to the beginning of the list and returns the node, which will then be pointed to by the beginning pointer in the function that calls this function */
Student *addStudent(const char firstname[], const char secondname[], Student *bgn) {

  Student *newStudent = (Student *) malloc(sizeof(Student));

  strcpy(newStudent->firstName, firstname);
  strcpy(newStudent->secondName, secondname);
  newStudent->studentID = ID;
  newStudent->next = bgn;
  ID++;

  return newStudent;
}

//this does the same as before but with books, and automatically sets its status to 1 making it available to borrow
Book *addBook(const char name[], const char bookAuthor[], Book *bgn) {
  Book *newBook = (Book *) malloc(sizeof(Book));

  strcpy(newBook->nameOfBook, name);
  strcpy(newBook->author, bookAuthor);
  newBook->status = 1;
  newBook->next = bgn;

  return newBook;
}

//this functions returns a pointer to the node which has the same book name as the one in the parameter given, and does this recursively. returns NULL if no list or book exists
Book *findBook(const char name[], Book *bgn) {
  if(bgn == NULL)
      return NULL;

  if(strcmp(bgn->nameOfBook,name) == 0)
      return bgn;

  findBook(name,bgn->next);
}

/*this function will use the above function to get the pointer to the book it wishes to remove and then removes this book
if there is no list, NULL is returned
if there is no book to remove, then null is returned
if the book is the beginning of the list, then a new beginning pointer is returned
else, iterate through list until correct node is found*/
Book *removeBook(Book *removal, Book *bgn) {
  if(bgn == NULL)
      return NULL;

  if(removal == NULL)
      return bgn;

  if(removal == bgn) {
      Book *newHead = bgn->next;
      free(removal);
      return newHead;
  }

  for(Book *current = bgn; current != NULL; current = current->next) {
      if(current->next == removal) {
          current->next = removal->next;
          free(removal);
      }
  }

  return bgn;
}
