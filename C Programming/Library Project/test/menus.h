#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <stdbool.h>
#include "DataStructure.h"
#include "Free.h"
#include "Print.h"
#include "addRemove.h"
#include "borrowReturn.h"
#include "readWriteFile.h"

Book *studentMain(Student *studentBgn, Book *bookBgn);

Book *librarianMain(Student *studentBgn, Book *bookBgn);

