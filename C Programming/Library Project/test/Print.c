#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include "DataStructure.h"


//these functions iterate through the lists and print the nodes for the user to see
void printAllBooks(const Book *bgn) {
  for (const Book *current = bgn; current != NULL; current = current->next)
    printf("%s by %s \n",current->nameOfBook, current->author);

  printf("END OF LIST \n\n");
}

//this function only prints if status is available
void printAvailableBooks(const Book *bgn) {
  for (const Book *current = bgn; current != NULL; current = current->next) {
    if(current->status == 1)
      printf("%s by %s \n",current->nameOfBook, current->author);
  }

  printf("END OF LIST \n\n");
}

void printStudents(const Student *bgn) {
  for (const Student *current = bgn; current != NULL; current = current->next)
    printf("Name: %s %s ID: %i\n",current->firstName, current->secondName, current->studentID);

  printf("END OF LIST \n\n");
}
