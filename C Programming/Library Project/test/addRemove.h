#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <stdbool.h>
#include "DataStructure.h"

Student *addStudent(const char firstname[], const char secondname[], Student *bgn);

Book *addBook(const char nameOfBook[], const char author[], Book *bgn);

Book *findBook(const char name[], Book *bgn);

Book *removeBook(Book *removal, Book *bgn);
