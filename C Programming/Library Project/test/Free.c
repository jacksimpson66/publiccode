#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include "DataStructure.h"

//these functions iterate through lists and free nodes
void deleteBooks(Book *bgn) {
  Book *temp;

  while (bgn != NULL) {
    temp = bgn;
    bgn = bgn->next;
    free(temp);
  }
}

void deleteStudent(Student *bgn) {
  Student *temp;

  while (bgn != NULL) {
    temp = bgn;
    bgn = bgn->next;
    free(temp);
  }
}
