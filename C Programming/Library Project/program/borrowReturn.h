#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <stdbool.h>
#include <string.h>
#include "DataStructure.h"

bool searchBookName(const char name[], Book *bgn);

void borrowBook(const char name[], Book *bgn);

void returnBook(const char name[], Book *bgn);
