#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <stdbool.h>
#include "DataStructure.h"
#include "Free.h"
#include "Print.h"
#include "addRemove.h"
#include "borrowReturn.h"
#include "readWriteFile.h"

//student main menu function

Book *studentMain(Student *studentBgn, Book *bookBgn) {
	
	int userChoice;
	//while loop keeps user in the program until they wish to quit
	while(1) {
		printf("\n\nHELLO STUDENT!\n\n");
		printf("Please choose an option:\n1.See available books\n2.Search for books\n3.Borrow Book\n4.Return Book\n5.Quit\nWhen searching, borrowing or returning books, please use the name of the book and spell correctly as shown in the list of books\n");

		scanf("%i", &userChoice);

		if(userChoice == 1 || userChoice == 2 || userChoice == 3 || userChoice == 4 || userChoice == 5) {
			//if user chooses 1 then available books are shown
			if(userChoice == 1) {
				printAvailableBooks(bookBgn);
			}
			//if user chooses 2 then they search for a book, and gives useful error message if book is spelt wrong
			if(userChoice == 2) {
				fgetc(stdin);
				printf("Please enter the name of the book:\n");
				char userInputSearch[256];
				fgets(userInputSearch, 256, stdin);
				int lenSearch = strlen(userInputSearch);
				userInputSearch[lenSearch-1] = '\0';
				bool searchResult = searchBookName(userInputSearch, bookBgn);
				if(searchResult == true)
					printf("Your book is supplied by the library, check the list of books to see if its available\n");
				if(searchResult == false)
					printf("The book is either unavailable or has been spelt wrong");
			}
			//if user chooses 3 then it borrows the book
			if(userChoice == 3) {
				fgetc(stdin);
				printf("Please enter the name of the book:\n");
				char userInputBorrow[256];
				fgets(userInputBorrow, 256, stdin);
				int lenBorrow = strlen(userInputBorrow);
				userInputBorrow[lenBorrow-1] = '\0';
				borrowBook(userInputBorrow, bookBgn);
			}
			//if user chooses 4 then it returns given book
			if(userChoice == 4) {
				fgetc(stdin);
				printf("Please enter the name of the book:\n");
				char userInputReturn[256];
				fgets(userInputReturn, 256, stdin);
				int lenReturn = strlen(userInputReturn);
				userInputReturn[lenReturn-1] = '\0';
				returnBook(userInputReturn, bookBgn);
			}
			if(userChoice == 5) 
				return bookBgn;
		}
		else
			printf("Incorrect number, Please choose again\n");
	}
	return bookBgn;
}

//function for librarian main menu

Book *librarianMain(Student *studentBgn, Book *bookBgn) {

	char password[50];
	//checks that password is correct and also gives option to exit
	while(1) {
		printf("Please enter the password (enter -99 to exit): ");
		scanf("%s", password);
		if(strcmp(password, "cricket1999") == 0)
			break;
		if(strcmp(password, "-99") == 0)
			return bookBgn;
		else
			printf("Password incorrect, try again.\n");
	}

	int userChoice;

	while(1) {
		printf("\nHELLO Librarian!\n\n");
		printf("Please choose an option:\n1.See all books\n2.See available books\n3.Add books\n4.Remove books\n5.See Students\n6.Quit\nWhen searching, borrowing or returning books, please use the name of the book and spell correctly as shown in the list of books\n");

		scanf("%i", &userChoice);

		if(userChoice == 1 || userChoice == 2 || userChoice == 3 || userChoice == 4 || userChoice == 5 || userChoice == 6) {
			//if user chooses 1 then all books shown
			if(userChoice == 1) {
				printAllBooks(bookBgn);
			}
			//if user chooses 2 then available books shown
			if(userChoice == 2) {
				printAvailableBooks(bookBgn);
			}
			//adds book with given book name and author names
			if(userChoice == 3) {
				fgetc(stdin);
				printf("Please enter the name of the book:\n");
				char userInputAddName[256];
				char userInputAddAuthor[256];
				fgets(userInputAddName, 256, stdin);
				printf("Please enter the author of the book:\n");
				fgets(userInputAddAuthor, 256, stdin);
				int lenName = strlen(userInputAddName);
				userInputAddName[lenName-1] = '\0';
				int lenAuthor = strlen(userInputAddAuthor);
				userInputAddAuthor[lenAuthor-1] = '\0';
				bookBgn = addBook(userInputAddName, userInputAddAuthor, bookBgn);
			}
			//removes book with given book name and displays useful error message if spelt wrong
			if(userChoice == 4) {
				fgetc(stdin);
				printf("Please enter the name of the book:\n");
				char userInputRemove[256];
				fgets(userInputRemove,256,stdin);
				int lenRemove = strlen(userInputRemove);
				userInputRemove[lenRemove-1] = '\0';
				if(findBook(userInputRemove, bookBgn) != NULL)
					bookBgn = removeBook(findBook(userInputRemove, bookBgn), bookBgn);
				else
					printf("Book does not exist, or is spelt wrong - remember capitals!");
			}
			//prints students if user chooses 5
			if(userChoice == 5)
				printStudents(studentBgn);
			if(userChoice == 6) 
				//exits
				return bookBgn;
		}
		else
			printf("Incorrect number, Please choose again\n");
	}
	return bookBgn;
}


