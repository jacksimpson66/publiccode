#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <stdbool.h>
#include "DataStructure.h"
#include "Free.h"
#include "menus.h"
#include "Print.h"
#include "addRemove.h"
#include "borrowReturn.h"
#include "readWriteFile.h"

void main() {

	//initalises linked lists for students and books
	Student *studentBgn = NULL;
	studentBgn = readFileStudent("students.csv", studentBgn);

	Book *bookBgn = NULL;
	bookBgn = readFileBook("books.csv", bookBgn);

	int userChoice;
	//while loop that keeps user in the program until they choose to quit, also allowing for incorrect numbers to be 
	while(1) {
		printf("\n\n<------------ HELLO! WELCOME TO THE LIBRARY ------------>\n\n");
		printf("Please choose an option:\n1. Librarian log in\n2.Student log in\n3.Sign up as a student\n4.Quit\n");

		scanf("%i", &userChoice);
	
		if(userChoice == 1 || userChoice == 2 || userChoice == 3 || userChoice == 4) {
			//if user chooses 1 then moves onto librarian main page
			if(userChoice == 1) {
				bookBgn = librarianMain(studentBgn, bookBgn);
				break;
			}
			//if user chooses 2 then moves onto student main page
			if(userChoice == 2) {
				bookBgn = studentMain(studentBgn, bookBgn);
				break;
			}
			//if user chooses 3 then begins to take details for signing up new student
			if(userChoice == 3) {
				//clears stdin stream
				fgetc(stdin);
				//uses fgets to collect names and then removes the newline character from the end
				char firstName[256];
				char secondName[256];
				printf("Please enter your first name: ");
				fgets(firstName, 256, stdin);
				printf("Please enter your second name: ");
				fgets(secondName, 256, stdin);
				int lenFirst = strlen(firstName);
				firstName[lenFirst-1] = '\0';
				int lenSecond = strlen(secondName);
				secondName[lenSecond-1] = '\0';
				//adds student to linked list
				studentBgn = addStudent(firstName, secondName, studentBgn);
				break;
			}	
			if(userChoice == 4)
				break;
		}
		else
			printf("Incorrect number, Please choose again\n");
	}
	//before program terminates, writes data back into file, overwriting it with new updated linked lists, and frees memory
	writeFileStudent("students.csv", studentBgn);
	writeFileBook("books.csv", bookBgn);

	deleteBooks(bookBgn);
	deleteStudent(studentBgn);
}
