#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include "DataStructure.h"
#include "addRemove.h"

Student *readFileStudent(const char filename[], Student *bgn);

Book *readFileBook(const char filename[], Book *bgn);

void writeFileStudent(const char filename[], Student *bgn);

void writeFileBook(const char filename[], Book *bgn);
