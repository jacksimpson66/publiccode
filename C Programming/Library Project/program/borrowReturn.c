#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <stdbool.h>
#include "DataStructure.h"

//iterates through book list and uses string compare function to see if book exists in the list, else false is returned
bool searchBookName(const char name[], Book *bgn) {
  Book *temp = bgn;

  while(temp != NULL) {
    if(strcmp(temp->nameOfBook, name) == 0)
      return true;
    temp = temp->next;
  }
  return false;
}

//iterates through the list and changes books status to unavailable if it exists and its status is currently available
void borrowBook(const char name[], Book *bgn) {
  Book *temp = bgn;

  while(temp != NULL) {
    if(strcmp(temp->nameOfBook, name) == 0 && temp->status == 1) {
      temp->status = 0;
      return;
    }
    temp = temp->next;
  }
  printf("\n<---The book does not exist, has been spelt wrong or is already taken-->\n");
  return;
}

//does the same as previous function but makes book available rather than unavailable
void returnBook(const char name[], Book *bgn) {
    Book *temp = bgn;

    while(temp != NULL) {
      if(strcmp(temp->nameOfBook, name) == 0 && temp->status == 0) {
        temp->status = 1;
        return;
      }
      temp = temp->next;
    }
    printf("\n<---The book does not exist, has been spelt wrong or has not been taken out-->\n");
    return;
}
