#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include "DataStructure.h"

void printAllBooks(const Book *bgn);

void printAvailableBooks(const Book *bgn);

void printStudents(const Student *bgn);
