#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include "DataStructure.h"
#include "addRemove.h"

/*takes filename and pointer to beginning of list as parameters
opens file and reads it
if file does not exist error is shown
else, csv file is parsed using strtok and added to list, making sure newline character is removed*/
Student *readFileStudent(const char filename[], Student *bgn) {
	FILE *data;
	data = fopen(filename, "r");

	if(data != NULL) {
		char parsedline[100];

		while(fgets(parsedline, 50, data) != NULL) {
			char *firstname = strtok(parsedline, ",");
			char *secondname = strtok(NULL, ",");
			int len = strlen(secondname);
			secondname[len-1] = '\0';
			bgn = addStudent(firstname, secondname, bgn);
		}

		fclose(data);
	}
	else {
		perror("fopen");
		printf("FILE DOES NOT EXIST\n");
	}
	return bgn;
}

//does the same as previous function but for books
Book *readFileBook(const char filename[], Book *bgn) {
	FILE *data;
	data = fopen(filename, "r");

	

	if(data != NULL) {

		char parsedline[200];

		while(fgets(parsedline, 50, data) != NULL) {
			char *bookname = strtok(parsedline, ",");
			char *author = strtok(NULL, ",");
			int len = strlen(author);
			author[len-1] = '\0';
			bgn = addBook(bookname, author, bgn);
		}

		fclose(data);
	}
	else {
		perror("fopen");
		printf("FILE DOES NOT EXIST\n");
	}
		
	return bgn;
}

/*opens file, given by parameter, and writes into it
iterates through lists and puts nodes into file, separating them with a comma*/
void writeFileStudent(const char filename[], Student *bgn) {
	FILE *file;
	file = fopen(filename, "w");

	for (const Student *current = bgn; current != NULL; current = current->next) {
		fputs(current->firstName, file);
		fputs(",", file);
		fputs(current->secondName, file);
		fputs("\n", file);
	}
	fclose(file);
}

//same as above function but for books
void writeFileBook(const char filename[], Book *bgn){
	FILE *file;
	file = fopen(filename, "w");

	for (const Book *current = bgn; current != NULL; current = current->next) {
		fputs(current->nameOfBook, file);
		fputs(",", file);
		fputs(current->author, file);
		fputs("\n", file);
	}
	fclose(file);
}




