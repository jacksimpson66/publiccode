#pragma once

//defines data structures for student and book nodes

struct student {
  char firstName[20];
  char secondName[20];
  int studentID;
  struct student *next;
};

typedef struct student Student;

struct book {
  char nameOfBook[20];
  char author[20];
  int status;
  struct book *next;
};

typedef struct book Book;
