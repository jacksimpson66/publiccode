import java.util.*;
import java.lang.Exception;

public class Deck extends CardCollection {


  public Deck() {

    for(Card.Suit s: Card.Suit.values()) {
      for(Card.Rank r : Card.Rank.values()) {
        Card temp = new Card(r,s);
        cards.add(temp);
      }
    }
  }

  public Card deal() {
    if(cards.isEmpty()==true){
      throw new CardException("The deck is empty");
    }
    else{
      Card c = cards.get(0);
      cards.remove(0);
      return c;
    }
  }

  // public void print() {
  //   for(Card c : cards) {
  //     System.out.println(c);
  //   }
  // }


  public void shuffle() {
    Collections.shuffle(cards);
  }
}
