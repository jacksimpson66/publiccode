import java.util.*;
import java.lang.*;

public class Game {

  public static void main(String[] args) {
    int rounds = 0;
    int dealerwins = 0;
    int userwins = 0;
    while(true){
      Scanner menuscan = new Scanner(System.in);
      System.out.println("Would you like to play a game? (y/n)");
      String menuinput = menuscan.next();
      if(menuinput.equals("n")) {
        System.out.println("Rounds played: " + rounds);
        System.out.println("Dealer wins: " + dealerwins);
        System.out.println("User wins: " + userwins);
        System.exit(1);
      }
      else {
        Deck deck = new Deck();
        deck.shuffle();
        BlackjackHand dealerhand = new BlackjackHand();
        BlackjackHand userhand = new BlackjackHand();

        Scanner scan = new Scanner(System.in);

        userhand.add(deck.deal());
        System.out.println(userhand.toString());

        boolean usernatural = false;
        boolean dealernatural = false;
        boolean userwon = false;
        boolean dealerwon = false;

        while(true) {
          System.out.println("[H]it or [S]tand");
          String input = scan.next();
          if(input.equals("h") == false && input.equals("s") == false) {
            System.out.println("Please enter h or s");
            continue;
          }
          if(input.equals("s")) {
            break;
          }
          else {
            userhand.add(deck.deal());
            System.out.println(userhand.toString());
            if(userhand.isBust()== true) {
              System.out.println("You're bust! The dealer wins!");
              dealerwon = true;
              dealerwins++;
              break;
            }
            if(userhand.isNatural()==true) {
              usernatural = true;
              break;
            }
          }
        }

        if(dealerwon == false) {
          while(dealerhand.value() < 17) {
            dealerhand.add(deck.deal());
            System.out.println(dealerhand.toString());
            if(dealerhand.isBust()==true) {
              System.out.println("Dealer is bust! You win!");
              userwon = true;
              userwins++;
              break;
            }
            if(dealerhand.isNatural()==true) {
              if(usernatural == false) {
                System.out.println("Dealer has a natural hand! They win!");
                dealerwon = true;
                dealerwins++;
                break;
              }
              if(usernatural == true) {
                System.out.println("You both have natural hands, you tie!");
                dealernatural = true;
                break;
              }
            }
          }
        }


        if(dealerwon == false && userwon == false) {
          if(usernatural == true && dealernatural == false) {
            System.out.println("You had a natural hand and the dealer didn't! You win!");
            userwon = true;
            userwins++;
          }
          else {
            if(userhand.value() > dealerhand.value()) {
              System.out.println("You had a higher value hand! You win!");
              userwins++;
            }
            if(userhand.value() < dealerhand.value()) {
              System.out.println("The dealer had a higher value hand! They win!");
              dealerwins++;
            }
            if(userhand.value() == dealerhand.value())
              System.out.println("The value of your hands are equal, you tie!");
          }
        }
      }
      rounds++;
    }
  }
}
