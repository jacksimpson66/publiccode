import java.util.*;
import java.lang.*;

public class BlackjackHand extends CardCollection {

  public BlackjackHand() {
    cards = new LinkedList<>();
  }

  public BlackjackHand(String s) {
    cards = new LinkedList<>();
    String str[] = s.split(" ");
    for(String string: str) {
      cards.add(new Card(string));
    }
  }

  public void discard(Deck deck) {
    if(cards.isEmpty() == true)
      throw new CardException("Blackjack Hand is empty");
    for(Card card : cards) {
      deck.add(card);
    }
    cards.clear();
  }

  // public void print() {
  //   for(Card c : cards) {
  //     System.out.println(c);
  //   }
  // }
  @Override
  public String toString() {
    String string = "";
    for(Card card: cards) {
      string += card + " ";
    }
    String newstring = string.trim();
    return newstring;
  }

  public boolean isBust() {
    if(value() > 21)
      return true;
    else
      return false;
  }


  public boolean isNatural() {
    if(cards.size() == 2 && value()==21)
      return true;
    else
      return false;
  }

  @Override
  public int value() {
    Card ac = new Card("AC");
    Card ad = new Card("AD");
    Card ah = new Card("AH");
    Card as = new Card("AS");
    int sum = 0;
    for (Card card: cards) {
      sum += card.value();
    }
    if(cards.contains(ac) == true || cards.contains(ad) == true || cards.contains(ah) == true || cards.contains(as) == true)
      if(sum + 10 <= 21) {
        sum += 10;
      }
    return sum;
  }

  @Override
  public void add(Card card) {
    if(isBust()==true)
      throw new CardException("Hand is bust");
    else
      cards.add(card);
  }

}
