// Correctness testing for COMP1721 Coursework 2 (Full Solution)

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;


public class FullTests {

  private BlackjackHand oneCard, twoCards, withAce;

  @Before
  public void setUp() {
    oneCard = new BlackjackHand();
    oneCard.add(new Card("3D"));

    twoCards = new BlackjackHand();
    twoCards.add(new Card("3D"));
    twoCards.add(new Card("7S"));

    withAce = new BlackjackHand();
    withAce.add(new Card("AC"));
    withAce.add(new Card("3D"));
  }

  @Test
  public void valueNoAces() {
    assertThat(oneCard.value(), is(3));
    assertThat(twoCards.value(), is(10));
  }

  @Test
  public void valueSoftAce() {
    assertThat(withAce.value(), is(14));
  }

  @Test
  public void valueHardAce() {
    withAce.add(new Card("9H"));
    assertThat(withAce.value(), is(13));
  }

  @Test
  public void isNatural() {
    withAce.add(new Card("7S"));

    BlackjackHand hand = new BlackjackHand();
    hand.add(new Card("AC"));
    hand.add(new Card("JH"));

    assertThat(oneCard.isNatural(), is(false));
    assertThat(twoCards.isNatural(), is(false));
    assertThat(withAce.size(), is(3));
    assertThat(withAce.value(), is(21));
    assertThat(withAce.isNatural(), is(false));
    assertThat(hand.size(), is(2));
    assertThat(hand.value(), is(21));
    assertThat(hand.isNatural(), is(true));
  }

  @Test
  public void notBust() {
    withAce.add(new Card("7S"));

    BlackjackHand hand = new BlackjackHand();
    hand.add(new Card("4D"));
    hand.add(new Card("7S"));
    hand.add(new Card("TH"));

    assertThat(twoCards.isBust(), is(false));
    assertThat(withAce.isBust(), is(false));
    assertThat(hand.isBust(), is(false));
  }

  @Test
  public void isBust() {
    BlackjackHand hand = new BlackjackHand();
    hand.add(new Card("5D"));
    hand.add(new Card("7S"));
    hand.add(new Card("TH"));

    assertThat(hand.isBust(), is(true));
  }

  @Test(expected=CardException.class)
  public void addToBustHand() {
    BlackjackHand hand = new BlackjackHand();
    hand.add(new Card("5D"));
    hand.add(new Card("7S"));
    hand.add(new Card("TH"));
    hand.add(new Card("2C"));  // exception should occur here
  }

  public static void main(String[] args) {
    JUnitCore core = new JUnitCore();
    core.addListener(new TestReporter());
    core.run(FullTests.class);
  }

}
