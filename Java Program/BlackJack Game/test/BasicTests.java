// Correctness testing for COMP1721 Coursework 2 (Basic Solution)

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;


public class BasicTests {

  private Deck deck;
  private BlackjackHand empty, oneCard, twoCards;

  @Before
  public void setUp() {
    Card.fancySymbols = false;
    deck = new Deck();

    empty = new BlackjackHand();

    oneCard = new BlackjackHand();
    oneCard.add(new Card("AC"));

    twoCards = new BlackjackHand();
    twoCards.add(new Card("AC"));
    twoCards.add(new Card("3D"));
  }

  // Deck class

  @Test
  public void deckSuperclass() {
    Class c = deck.getClass().getSuperclass();
    assertThat(c.getName(), is("CardCollection"));
  }

  @Test
  public void fullDeck() {
    assertThat(deck.size(), is(52));
  }

  @Test
  public void dealFullDeck() {
    Card c = deck.deal();
    assertThat(deck.size(), is(51));
    assertThat(c.getRank(), is(Card.Rank.ACE));
    assertThat(c.getSuit(), is(Card.Suit.CLUBS));
    c = deck.deal();
    assertThat(deck.size(), is(50));
    assertThat(c.getRank(), is(Card.Rank.TWO));
    assertThat(c.getSuit(), is(Card.Suit.CLUBS));
  }

  @Test(expected=CardException.class)
  public void dealEmptyDeck() {
    deck.discard();
    deck.deal();
  }

  // BlackjackHand class

  @Test
  public void handSuperclass() {
    Class c = empty.getClass().getSuperclass();
    assertThat(c.getName(), is("CardCollection"));
  }

  @Test
  public void addingCards() {
    assertThat(empty.size(), is(0));
    assertThat(oneCard.size(), is(1));
    assertThat(twoCards.size(), is(2));
  }

  @Test
  public void creationFromStringOneCard() {
    BlackjackHand hand = new BlackjackHand("AC");
    assertThat(hand.size(), is(1));
  }

  @Test
  public void creationFromStringTwoCards() {
    BlackjackHand hand = new BlackjackHand("AC 3D");
    assertThat(hand.size(), is(2));
  }

  @Test
  public void discardToDeck() {
    BlackjackHand hand = new BlackjackHand();
    hand.add(deck.deal());
    hand.add(deck.deal());
    assertThat(hand.size(), is(2));
    assertThat(deck.size(), is(50));
    hand.discard(deck);
    assertThat(hand.size(), is(0));
    assertThat(deck.size(), is(52));
  }

  @Test(expected=CardException.class)
  public void discardToDeckEmptyHand() {
    empty.discard(deck);
  }

  @Test
  public void handAsString() {
    assertThat(empty.toString(), is(""));
    assertThat(oneCard.toString(), is("AC"));
    assertThat(twoCards.toString(), is("AC 3D"));
  }

  public static void main(String[] args) {
    JUnitCore core = new JUnitCore();
    core.addListener(new TestReporter());
    core.run(BasicTests.class);
  }

}
